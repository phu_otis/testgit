package com.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Setter
@Getter
@Table(name ="points")

public class Point  {
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    private Integer id;
    @JsonProperty
    @Range(min = 0, max = 10)
    private Double point;
    @JsonProperty
    @Range(min =1, max = 12)
    private Integer month;
    @JsonProperty
    @Range(min = 2001, max = 2021)
    private Integer year;
    @JsonProperty
    private String createTime;
    @JsonProperty
    private String updateTime;
    @ManyToOne
    @JoinColumn(name = "userId", referencedColumnName = "id")
    private User user;
}
