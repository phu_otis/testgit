package com.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.*;

@Entity
@Setter
@Getter
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;
    @JsonProperty
    private String email;
    @JsonProperty
    @Column(length = 2000)
    private String password;
    @JsonProperty
    @Column(length = 255)
    private String name;
    @JsonProperty
    @Range(min = 0, max = 150)
    private Integer age;
    @JsonProperty
    private String gender;
    @JsonProperty
    private  Boolean isMember;
    @JsonProperty
    private  Boolean isActive;
    @JsonProperty
    private String createTime;
    @JsonProperty
    private String updateTime;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Point> points = new ArrayList<>();
}
