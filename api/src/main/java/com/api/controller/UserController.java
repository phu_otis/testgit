package com.api.controller;


import com.api.entity.User;
import com.api.service.IPointService;
import com.api.service.IUserService;
import com.sun.source.tree.TryTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin
@RestController
@RequestMapping(path = "/api/user")
public class UserController {
    @Autowired
    private IUserService userService;
    private IPointService iPointServiceInClassUser;

    // add create user api
    @PostMapping(path = "/create")
    public @ResponseBody
    ResponseEntity<String> NewUser(@RequestParam String email,
                                       @RequestParam String password,
                                       @RequestParam String name,
                                       @RequestParam Integer age,
                                       @RequestParam String gender,
                                       @RequestParam Boolean isMember,
                                       @RequestParam Boolean isActive
    )

    {

        User us = new User();
        try{
            us.setEmail(email);
            us.setPassword(userService.md5(password));
            us.setName(name);
            us.setAge(age);
            us.setGender(gender);
            us.setIsMember(isMember);
            us.setIsActive(isActive);

            us.setCreateTime(userService.createTime());
            us.setUpdateTime(userService.updateTime());

            if(userService.checkEmail(us.getEmail())){
                userService.save(us);
                return new ResponseEntity<>("Add successful",HttpStatus.valueOf(200));
            }
            else {
                return new ResponseEntity<>("email invalid",HttpStatus.valueOf(200));
            }
        }
        catch (Exception e){
            return new ResponseEntity<>("Something wrong with your data",HttpStatus.valueOf(200));
        }
        }

    //get all user api
    @GetMapping(path = "/get_all")
    List<User> GetAllUser() {

        return userService.findAll();

    }

    @DeleteMapping(path = "/delete/{id}")
    ResponseEntity<String> DeleteUser(@PathVariable Integer id) {
        try {
            userService.deleteById(id);
            //iPointServiceInClassUser.deleteByIdAfterDeleteUser(id);
            return new ResponseEntity<>("Delete successful",HttpStatus.valueOf(200));
        }
        catch (Exception e){
            return new ResponseEntity<>("Delete failed",HttpStatus.valueOf(200));

        }


    }
    //PUT
    @PutMapping(path = "/update/{Id}/{newPassword}")
    public @ResponseBody
    ResponseEntity<String> updatePass(@PathVariable String newPassword,@PathVariable Integer Id){
        try {
            userService.updatePassword(userService.md5(newPassword), Id);
            return new ResponseEntity<>("update successful",HttpStatus.valueOf(200));
        }
        catch (Exception e){
            return new ResponseEntity<>("update fail",HttpStatus.valueOf(200));

        }

    }

}