package com.api.controller;

import com.api.entity.Point;
import com.api.entity.Temp;
import com.api.service.IPointService;
import com.api.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.api.entity.User;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "/api/point")
public class PointController {
    @Autowired
    private IPointService pointService;
    // add create user api
    private IUserService userService;

    @PostMapping(path = "/create")
    public @ResponseBody
    ResponseEntity<String> NewPoint(@RequestParam Integer month,
                                        @RequestParam Integer year,
                                        @RequestBody Temp temp)
    {
        try{
            Point pt = new Point();
            pt.setMonth(month);
            pt.setPoint(temp.getPoint());
            pt.setYear(year);
            pt.setCreateTime(pointService.createTime());
            pt.setUpdateTime(pointService.updateTime());

//            pt.setUser(userService.getOne(temp.getUserId()));
            pointService.save(pt);
            return new ResponseEntity<>("Add successful",HttpStatus.valueOf(200));
        }
        catch (Exception e){
            return new ResponseEntity<>("Something wrong with your data",HttpStatus.valueOf(200));
        }
    }
    // get all point
    @GetMapping(path = "/get_all")
    List<Point> GetAllPoint() {
        return pointService.findAll();
    }
    //Get total point
    @GetMapping(path = "/get_total/{userId}/{year}")
    double getTotalPoint(@PathVariable Integer userId,@PathVariable Integer year){
        try{
            return pointService.getTotalPoint(userId, year);
        }
        catch (Exception e){
            return  0;
        }

    }
    // create point by month
    @PutMapping(path = "/insert_to_id/{userId}")
    public  @ResponseBody
    ResponseEntity<String> createPointByMonth(@RequestParam Double point,
                                                  @RequestParam Integer month,
                                                  @PathVariable Integer userId){
        try {
            pointService.createPointByMonth(point,month,userId);
            return new ResponseEntity<>("Create successful",HttpStatus.valueOf(200));
        }
        catch (Exception e){
            return new ResponseEntity<>("Create fail",HttpStatus.valueOf(200));
        }
    }
    @DeleteMapping(path = "/delete/{id}")
    ResponseEntity<String> DeleteUser(@PathVariable Integer id) {
        try {
            pointService.deleteById(id);
            return new ResponseEntity<>("Delete successful",HttpStatus.valueOf(200));
        }
        catch (Exception e){
            return new ResponseEntity<>("Delete fail",HttpStatus.valueOf(200));
        }
    }
    // POST





}
