package com.api.service;


import com.api.entity.User;
import java.math.BigInteger;
import com.api.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiredArgsConstructor
@Service
public class UserService implements IUserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> findAll() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void deleteById(Integer id) {
        userRepository.deleteById(id);
    }

    @Override
    public Optional<User> findById(Integer id) {
        return userRepository.findById(id);
    }



    @Override
    public void updatePassword(String newPassword, Integer id) {

        userRepository.updatePassword(newPassword,id);
    }

    @Override
    public String md5(String str) {
        String result ="";
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.update(str.getBytes());
            BigInteger bigInteger = new BigInteger(1,digest.digest());
            result = bigInteger.toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return result;
    }
    @Override
    public String createTime() {
        LocalDate now =LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-YYYY");
        String formatDateTime = formatter.format(now);
        return formatDateTime;
    }
    @Override
    public String updateTime(){

        LocalDate now =LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-YYYY");
        String formatDateTime = formatter.format(now);
        return formatDateTime;
    }


    @Override
    public boolean checkEmail(String email) {
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public boolean isMatchPassword(String password, Integer id) {
        String comparePassword = md5(password);
        Optional<User> user = findById(id);
        if(user.get().getPassword() == comparePassword){
            return true;
        }
        else
            return false;
    }

    @Override
    public User getOne(Integer id){
        return userRepository.getOne(id);
    }
}