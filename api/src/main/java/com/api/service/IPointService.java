package com.api.service;
import com.api.entity.*;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
public interface IPointService {
    List<Point> findAll();
    Optional<Point> findById(Integer Id);
    void deleteById(Integer id);
    Point save(Point point);
    //int getTotalPoint();

    double getTotalPoint(Integer userId, int year);
    public String createTime();
    public String updateTime();
    void createPointByMonth(Double point, Integer month,Integer userId);
    //void deleteByIdAfterDeleteUser(Integer userId);


}
