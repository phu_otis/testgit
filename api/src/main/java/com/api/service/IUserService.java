package com.api.service;


import com.api.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import  com.api.repository.UserRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
@Transactional
public interface IUserService {
    List<User> findAll();

    Optional<User> findById(Integer id);

    //User changePassWord(User user);
    void save(User user);
    User getOne(Integer id);


    void deleteById(Integer id);
    String md5(String str);

    void updatePassword(String newPassword, Integer id);
    public String createTime();
    public String updateTime();
    public boolean checkEmail(String email);
    public boolean isMatchPassword(String password, Integer id);
    //String encodePassword(String password);

}