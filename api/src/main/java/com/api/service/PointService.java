package com.api.service;



import com.api.entity.Point;
import com.api.repository.PointRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import java.time.format.DateTimeFormatter;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor

public class PointService implements IPointService {
    @Autowired
    private PointRepository pointRepository;

    @Override
    public List<Point> findAll() {

        return (List<Point>) pointRepository.findAll();
    }

    @Override
    public Optional<Point> findById(Integer id) {
        return pointRepository.findById(id);
    }

    @Override
    public Point save(Point point) {
        return pointRepository.save(point);
    }

    @Override
    public void deleteById(Integer id) {
        pointRepository.deleteById(id);
    }

    @Override
    public double getTotalPoint(Integer userId, int year) {
        return pointRepository.getTotalPoint(userId,year);
    }

    @Override
    public String createTime() {
        LocalDate now =LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-YYYY");
        String formatDateTime = formatter.format(now);
        return formatDateTime;
    }
    @Override
    public String updateTime(){

        LocalDate now =LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-YYYY");
        String formatDateTime = formatter.format(now);
        return formatDateTime;
    }

    @Override
    public void createPointByMonth(Double point, Integer month,Integer userId) {
        pointRepository.createPointByMonth(point,month,userId);
    }

//    @Override
//    public void deleteByIdAfterDeleteUser(Integer userId) {
//        pointRepository.deleteByIdAfterDeleteUser(userId);
//    }
}

