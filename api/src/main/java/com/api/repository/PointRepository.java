package com.api.repository;

import com.api.entity.Point;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import  org.hibernate.exception.*;


public interface PointRepository extends JpaRepository<Point,Integer> {

    @Transactional
    @Query("select sum(point.point)from Point point WHERE point.user.id= :userId and point.year =:year")
    double getTotalPoint(Integer userId,Integer year);

    @Query("update Point point set point.point =:point where point.month =:month and point.user.id = :userId")
    void createPointByMonth(Double point, Integer month,Integer userId);
//    @Query("delete from Point point where point.user.id =:userId")
//    void deleteByIdAfterDeleteUser(Integer userId);
}