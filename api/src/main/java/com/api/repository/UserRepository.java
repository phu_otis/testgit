package com.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.exception.*;
import com.api.entity.*;
public interface UserRepository extends JpaRepository<User,Integer> {

    @Transactional

    @Query("update User user set user.password = :newPassword where user.id = :id")
    void updatePassword(String newPassword,Integer id);


}
